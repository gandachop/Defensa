package com.ucbcba;

import java.util.ArrayList;
import java.util.List;

public class Comentario {
    private String texto;
    private int likes;
    private Usuario usuario;
    private List<Usuario> usuarios;

    public Comentario(){
        this.likes = 0;
    }
    public Comentario(String texto, Usuario usuario){
        this.likes = 0;
        this.texto = texto;
        this.usuario = usuario;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isPalabra(String palabra){
        /*
        String caracteres  = "abcdefghijklmnopqrs";
        for(int i=0; i < palabra.length(); i++){
            if(caracteres.contains(String.valueOf(palabra.charAt(i)))){
                return true;
            }
        }
        return false;*/
        return palabra.matches("(a-zA-Z)");
    }

    public int numeroPalabras(){

        String[] arreglo = this.texto.split(" ");
        int contador = 0;
        for(String palabra : arreglo){
            if(isPalabra(palabra)){
                contador++;
            }
        }
        return contador;
    }
    public int numeroLetras(){
        int cont=0;
        for(int i=0;i<texto.length();i++)
        {
            if((texto.charAt(i)>=97 && texto.charAt(i)<=122)||(texto.charAt(i)>=65 && texto.charAt(i)<=90)){
                cont++;
            }
        }
        return cont;
    }
    public int numNumeros(){
        int cont=0;
        for(int i=0;i<texto.length();i++)
        {
            if(texto.charAt(i)>='0'&&texto.charAt(i)<='9') {
                cont++;
            }
        }
        return cont;
    }
    public int numCarEspeciales(){
        int cont=0;
        for(int i=0;i<texto.length();i++){
            if(!(texto.charAt(i)>=97 && texto.charAt(i)<=122)||(texto.charAt(i)>=65 && texto.charAt(i)<=90)||!(texto.charAt(i)>='0'&&texto.charAt(i)<='9')){
                cont++;
            }
        }
        return cont;
    }

    public void like(Usuario usuario){
        usuarios=new ArrayList<Usuario>();
        this.likes++;
    }

}
