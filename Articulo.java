package com.ucbcba;

import java.util.ArrayList;
import java.util.List;

public class Articulo {

    private String texto;
    private int likes;
    private List<Comentario> comentarios;
    private Usuario usuario;
    private List<Usuario> usuarios;

    public Articulo(Usuario usuario){
        this.likes = 0;
        this.usuario= usuario;
    }
    public Articulo(String texto){
        this.likes = 0;
        this.texto = texto;
    }

    public int numeroComentarios(){
        return this.comentarios.size();
    }

    public boolean isPalabra(String palabra){
        /*
        String caracteres  = "abcdefghijklmnopqrs";
        for(int i=0; i < palabra.length(); i++){
            if(caracteres.contains(String.valueOf(palabra.charAt(i)))){
                return true;
            }
        }
        return false;*/
        return palabra.matches("(a-zA-Z)");
    }

    public int numeroPalabras(){
        String[] arreglo = this.texto.split(" ");
        int contador = 0;
        for(String palabra : arreglo){
            if(isPalabra(palabra)){
                contador++;
            }
        }
        for(Comentario com:comentarios)
        {
            contador+= com.numeroPalabras();
        }
        return contador;
    }
    public int numeroLetras(){
        int cont=0;
        for(int i=0;i<texto.length();i++)
        {
            if((texto.charAt(i)>=97 && texto.charAt(i)<=122)||(texto.charAt(i)>=65 && texto.charAt(i)<=90)){
                cont++;
            }
        }
        for(Comentario com:comentarios)
        {
            cont+=com.numeroLetras();
        }
        return cont;


    }
    public int numeroNumeros(){
        int cont=0;
        for(int i=0;i<texto.length();i++)
        {
            if(texto.charAt(i)>='0'&&texto.charAt(i)<='9') {
                cont++;
            }
        }
        for(Comentario com:comentarios)
        {
            cont+=com.numNumeros();
        }
        return cont;
    }
    public int numeroCarEspeciales(){
        int cont=0;
        for(int i=0;i<texto.length();i++){
            if(!(texto.charAt(i)>=97 && texto.charAt(i)<=122)||(texto.charAt(i)>=65 && texto.charAt(i)<=90)||!(texto.charAt(i)>='0'&&texto.charAt(i)<='9')){
                cont++;
            }
        }
        for(Comentario com:comentarios){
            cont+=com.numCarEspeciales();
        }
        return cont;
    }


    public void like(Usuario usuario){
        usuarios=new ArrayList<Usuario>();
        this.likes++;
    }

    public void dislike(String nom){
        String[]usuarios= ;
        for(int i=0; i<;i++)
        {
            nom.equals();
            this.likes--;
            System.out.println("User not found");
        }

    }

    public int getLikes() { return this.usuarios.size(); }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public List<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public void anadirComentario(Comentario comentario){this.comentarios.add(comentario);}

    public void borrarComentario(Integer id){
        this.comentarios.remove(id);
    }

}
