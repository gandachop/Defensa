package com.ucbcba;

import java.util.ArrayList;
import java.util.List;

public class Blog extends SitioWeb {

    private List<Articulo> articulos;
    private Usuario usuario;

    public Blog(String url, String nombre, Usuario u) {
        super(url, nombre);
        articulos = new ArrayList<Articulo>();
        this.usuario=u;
    }

    public List<Articulo> getArticulos() {
        return articulos;
    }

    public void setArticulos(List<Articulo> articulos) {
        this.articulos = articulos;
    }

    public int numeroArticulos(){
        return articulos.size();
    }
    public int numeroComentarios(){
        int numeroComentarios = 0;
        for(Articulo articulo : this.articulos){
            numeroComentarios += articulo.numeroComentarios();
        }
        return numeroComentarios;//TODO
    }

    public int numeroPalabras(){
        int cont=0;
        for(Articulo a: articulos)
        {
            cont+=a.numeroPalabras();
        }
        return cont;
    }
    public int numeroLetras(){
        int cont=0;
        for(Articulo a: articulos)
        {
            cont+=a.numeroLetras();
        }
        return cont;
    }
    public int numeroNumeros(){
        int cont=0;
        for (Articulo a: articulos){
            cont+=a.numeroNumeros();
        }
        return cont;
    }
    public int numeroCarEspeciales(){
        int cont=0;
        for (Articulo a: articulos ){
                cont+=a.numeroCarEspeciales();
        }
        return cont;
    }
    public void mostrarArticulos(){

        //TODO
    }

    public void anadirArticulo(Articulo articulo){
        this.articulos.add(articulo);
    }

    public void borrarArticulo(int id){
        this.articulos.remove(id);
    }
}
