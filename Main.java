package com.ucbcba;

public class Main {

    public static void main(String[] args)  {
	// write your code here
        Blog blog = new Blog("miblog.com", "Mi Blog",(new Usuario("Andres","Fuentes")));
        Articulo articulo1 = new Articulo("texto del articulo");
        articulo1.anadirComentario(new Comentario("Comentario",(new Usuario("Pablo","Jimenez"))));
        articulo1.like(new Usuario("Pepe","Gutierrez"));
        articulo1.like(new Usuario("Jose","Perez"));
        articulo1.dislike("Pepe");
        blog.anadirArticulo(articulo1);
        blog.anadirArticulo(new Articulo("Otro Texto"));
        blog.borrarArticulo(1);
        System.out.println(blog.numeroArticulos());
    ;

    }

}
